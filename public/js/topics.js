
function saveTopic(elm){
    var id = $(elm).prop("id");
    $(".topic_text_error").addClass("hidden");
    $(".topic_details_error").addClass("hidden");

    if(!id){
        id = 0;
    }

    var data ={};
    data.topic =  $("#topictext").val();
    data.id = id;
    data.details = $("#topicdetails").val();

    if(!data.topic || !data.details){
        if(!data.topic){
            $(".topic_text_error").removeClass("hidden");
        }

        if(!data.details){
            $(".topic_details_error").removeClass("hidden");
        }

        return false;
    }

    $.post("/topics/add",data, function( data ) {
        var htmlStr = "<li id='topic_id_TOPICID'><a data-ajax='false' href='/comments/TOPICID'><h2>"+data.topic+" </h2><p><strong class='topic_text'>"+data.details+"</strong></p></a><a onclick='javascript:deleteConfirm(this);' href='javascript:void(0);' id='"+data.id+"' >Delete Topic</a></li>";
        htmlStr =  htmlStr.replace(/\TOPICID/g,data.id);
        $(".topic_list").append(htmlStr);
        $(".topic_list").listview("refresh");

        $("#add_topic").popup("close");
        $("#topictext").val("");
        $("#topicdetails").val("");
        $("#no_data").hide();
    }).always(function(){
        $("#add_topic" ).popup("close");
    });

    return true;
}

function deleteConfirm(elm){
    var elm = $(elm);
    var id = elm.attr("id");

    $(".delete_topic").attr("id",id);

    $("#delete_topic" ).popup( "open", {positionTo:"window"} );
}

function deleteTopic(elm){
    var elm = $(elm);
    var id = elm.prop("id");

    $.ajax({
        url: "/topics/delete/" + id,
        type: 'DELETE',
        dataType:'JSON',
        success: function(data){
            $("#topic_id_"+id).remove();
        },
        error: function(){
            console.log("error");
        }
    });

}