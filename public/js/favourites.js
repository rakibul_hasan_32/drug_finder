var SelectedLi ='' ;

$('[href=#favouritedetails]').on('click',function (e) {
    // store parent of clicked button
    SelectedLi = $(this).parent();
});

$('[href=#delFavouriteItem]').on('click',function (e) {
    // store parent of clicked button

    var id =SelectedLi.attr("id");

    $.ajax({
        url: "/favoritedetails/" + id,
        type: 'DELETE',
        dataType:'JSON',
        success: function(data){
            SelectedLi.remove();
        },
        error: function(){
            console.log("error");
        }
    });
});


$('[href=#favourite]').on('click',function (e) {
    // store parent of clicked button
    SelectedLi = $(this).parent();
});

$('[href=#delFavourite]').on('click',function (e) {
    // store parent of clicked button

    var id =SelectedLi.attr("id");

    $.ajax({
        url: "/favorites/" + id,
        type: 'DELETE',
        dataType:'JSON',
        success: function(data){
            SelectedLi.remove();
        },
        error: function(){
            console.log("error");
        }
    });
});

function validateFavourite(){

    var note = $("#note").val()

    if(!note){
        $(".note_text_error").removeClass("hidden");
        return false;
    }

    return true;
}