<?php namespace App\Http\Controllers;

use App\Http\Repository\TopicRepository;
use App\Http\Requests;
use App\Http\Controllers\Controller AS BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;

class TopicController extends BaseController
{
    protected $topicRepo;
    protected $user;
    protected $id;


    /**
     *
     * @param $topicRepo $topicRepo
     */
    public function __construct(TopicRepository $topicRepo)
    {
        $this->topicRepo = $topicRepo;

        if (!Auth::guest()) {

            $this->user = Auth::user();
            $this->id = $this->user->id;
        }
    }


    public function index()
    {
        if (Auth::guest()) {
            return redirect('login');
        }

        $results = $this->topicRepo->getTopics();
        $results = collect($results);

        return view('forum.topics', ["topicList" => $results]);
    }

    public function getTopics()
    {
        if (Auth::guest()) {
            return redirect('login');
        }


        $results = $this->topicRepo->getTopics();
        $results = collect($results);

        return response()->json($results);
    }

    /**
     *
     * @param topic obj
     * @return topic obj
     */

    public function add()
    {
        if (Auth::guest()) {
            return redirect('login');
        }

        $params = Input::all();


        if (!isset($params['topic']) || !isset($params['details'])) {
            return response()->json(['message' => 'Required parameter missing']);
        }

        $params['created_by'] = $this->id;
        $results = $this->topicRepo->saveTopic($params);

        return response()->json($results);
    }

    /**
     *
     * @param topic id
     * @return topic obj
     */

    public function deleteTopic($topicId)
    {
        if (Auth::guest()) {
            return redirect('login');
        }

        if (!isset($topicId)) {
            return response()->json(['message' => 'Required parameter missing']);
        }

        $results = $this->topicRepo->deleteTopic($topicId);

        return response()->json($results);
    }

}
