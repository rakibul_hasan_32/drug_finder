<?php namespace App\Http\Controllers;

use App\Http\Repository\FavouriteRepository;
use App\Http\Requests;
use App\Http\Controllers\Controller AS BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;

use App\Http\Controllers\SearchController;

class FavouriteController extends BaseController
{
    protected $favouriteRepo;
    protected $user;
    protected $userid;

    /**
     *
     * @param $topicRepo $topicRepo
     */
    public function __construct(FavouriteRepository $favouriteRepo)
    {
        $this->favouriteRepo = $favouriteRepo;

        if (!Auth::guest()) {

            $this->user = Auth::user();
            $this->userid = $this->user->id;
        }
    }

    public function getFavouriteList()
    {
        $data = array();
        if (Auth::guest()) {
            return redirect('login');
        }

        $results = $this->favouriteRepo->getFavourites( $this->userid ) ;

        $results =  collect($results) ;

        $data["header"] = "Favourite List will be shown here";
        $data["favouritelist"] = $results;

        return view('favourite.favourite', $data );
    }

    public function getFavouriteDetails($favourite_id)
    {
        $data = array();
        if (Auth::guest()) {
            return redirect('login');
        }

        $results = $this->favouriteRepo->getFavouriteDeatils( $this->userid, $favourite_id ) ;

        //$results =  collect($results) ;

        $data["header"] =collect( $results['favourite'])['notes'] ;

        $data["favouriteslist"] = collect( $results["favouritesdetails"] );

        return view('favourite.favouritedetails', $data );
    }

    public function  addFavourites(Request $request)
    {
        if (Auth::guest()) {
            return redirect('login');
        }

        $note = $request->input('note');
        $search_term = $request->input('term');
        $search_results = app('App\Http\Controllers\SearchController')->getSearchResult( $search_term);

        $this->favouriteRepo->addFavourites($this->userid,
            $note, $search_term , $search_results);

        return redirect("favorites");
    }

    public function deleteFavouriteItem($favourite_id) {

        $results=false;

        if (Auth::guest()) {
            return redirect('login');
        }

        $results = $this->favouriteRepo->deleteFavouriteItem( $favourite_id ) ;

        return response()->json(['success' => $results ]);
    }

    public function deleteFavourite($favourite_id) {

        $results=false;

        if (Auth::guest()) {
            return response()->json(['message' => 'Access denied.','success'=>'false']);
        }

        $results = $this->favouriteRepo->deleteFavourite( $favourite_id ) ;

        return response()->json(['success' => $results ]);
    }

}