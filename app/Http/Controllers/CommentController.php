<?php namespace App\Http\Controllers;

use App\Http\Repository\CommentRepository;
use App\Http\Repository\TopicRepository;
use App\Http\Requests;
use App\Http\Controllers\Controller AS BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;

class CommentController extends BaseController
{
    protected $commentRepo;
    protected $user;
    protected $id;


    /**
     *
     * @param $commentRepo $commentRepo
     */
    public function __construct(CommentRepository $commentRepo)
    {
        $this->commentRepo = $commentRepo;

        if (!Auth::guest()) {

            $this->user = Auth::user();
            $this->id = $this->user->id;
        }
    }

    /**
     *
     * @param commentId commentId
     * @return comments array
     */

    public function index($topicId)
    {
        if (!isset($topicId)) {
            return response()->json(['message' => 'Topic id is not provided.']);
        }

        $results = $this->commentRepo->getComments($topicId);
        $topicRepo = new TopicRepository();
        $topic = $topicRepo->getTopic($topicId);
        $results = collect($results);

        return view('forum.comments', ["commentList" => $results,"topic"=>$topic]);
    }

    public function getComments($topicId)
    {
        if (Auth::guest()) {
            return redirect('login');
        }

        $results = $this->commentRepo->getComments($topicId);
        $results = collect($results);

        return response()->json($results);
    }

    /**
     *
     * @param comment obj
     * @return comments array
     */

    public function add()
    {
        if (Auth::guest()) {
            return redirect('login');
        }

        $params = Input::all();


        if (!isset($params['commenttext'])) {
            return response()->json(['message' => 'Required parameter missing']);
        }

        $params['created_by'] = $this->id; //TO DO: later we will change it to userid
        $results = $this->commentRepo->saveComment($params);

        return response()->json($results);
    }

    /**
     *
     * @param comment id
     * @return comments array
     */

    public function deleteComment($commentId)
    {
        if (Auth::guest()) {
            return redirect('login');
        }

        if (!isset($commentId)) {
            return response()->json(['message' => 'Required parameter missing']);
        }

        $results = $this->commentRepo->deleteComment($commentId);

        return response()->json($results);
    }

}
