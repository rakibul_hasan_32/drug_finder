<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller AS BaseController;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;


class SearchController extends BaseController
{

    public function __construct()
    {

    }

    public function index()
    {
        $data = array();
        $data['productList'] = array();
        $data['header'] = 'Search';
        $data['search_term'] = '';
        return view('search', $data);
    }

    public function getSearchResult($search_term){

        if ($search_term != "") {

            $url = "https://developer.pillfill.com:443/service/v1/products";

            $urlData = array();
            $headers = array();
            $headers['api_key'] = '92a56ac1f3a69e47c68f3cae207f2a87';

            $queryParam = array();
            $queryParam['term'] = $search_term;
            $queryParam['type'] = 'name';
            $queryParam['page'] = '0';


            $urlData['headers'] = $headers;
            $urlData['query'] = $queryParam;

            $client = new Client();
            $response = $client->request('GET', $url, $urlData);

            if ($response->getStatusCode() == 200) {
                return json_decode($response->getBody(), true);
            }
        }

        return null;

   }

    public function search(Request $request)
    {

        $data = array();
        $apiResponse = array();
        $search_term = $request->input('term');

        $data['header'] = 'Search results for "'.$search_term.'"';

        $data['search_term'] = $search_term;

        $apiResponse = $this->getSearchResult( $search_term  );

        if($apiResponse != null){
            $data['productList'] = $this->generateSearchList($apiResponse);
        } else {
            $data['error'] = "Search Api server is down" ;
        }
        $data['pagename'] = "search";
        return view('search', $data);
    }

    private function generateSearchList($productList)
    {
        $searchList = array();

        foreach ($productList as $product)
        {
            $tempData = array();
            $tempData['manufacturer'] = $product['manufacturer'];
            $tempData['item'] = $product['product']['name'];
            $tempData['genericMedicine'] = $product['product']['genericMedicine'];
            $tempData['form'] = $product['product']['form'];
            $tempData['splId'] = $product['splId'];
            array_push($searchList, $tempData);
        }
        return $searchList;
    }
}