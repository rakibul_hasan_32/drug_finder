<?php

namespace App\Http\Repository;


class FavouriteRepository
{
    /** @var DB */
    protected $db;


    public function __construct()
    {
        $this->db = \DB::connection('mysql');
    }

    public function getFavourites($user_id)
    {

            $query = $this->db->table('favourites')
                ->select(array(
                    $this->db->raw('favourites.id'),
                    $this->db->raw('favourites.created_by'),
                    $this->db->raw('favourites.created_at'),
                    $this->db->raw('favourites.notes'),
                    $this->db->raw('favourites.searchjson')
                    )
                )
                ->join('users', 'favourites.created_by', '=', 'users.id')
                ->where('users.id', "=", $user_id);

            $results = $query->get();

        return $results;
    }

    public function getFavouriteDeatils($user_id, $favourite_id )
    {

        $results = array();
        $query = $this->db->table('favourites')
            ->select(array(
                    $this->db->raw('favourites.id'),
                    $this->db->raw('favourites.created_by'),
                    $this->db->raw('favourites.created_at'),
                    $this->db->raw('favourites.notes'),
                    $this->db->raw('favourites.searchjson')
                )
            )
            ->join('users', 'favourites.created_by', '=', 'users.id')
            ->where('favourites.id','=', $favourite_id )
            ->where('users.id', "=", $user_id);

        $results['favourite']  =  $query->first();
        if( $results['favourite'] !=null ){

            $query = $this->db->table('favourite_details')
                ->select(array(
                        $this->db->raw('favourite_details.id'),
                        $this->db->raw('favourite_details.manufacturer'),
                        $this->db->raw('favourite_details.item_name'),
                        $this->db->raw('favourite_details.item_gen'),
                        $this->db->raw('favourite_details.item_form')
                    )
                )
                ->join('favourites', 'favourite_details.f_id', '=', 'favourites.id')
                ->where('favourite_details.f_id', "=", $favourite_id);

            $results['favouritesdetails'] = $query->get();

        }

        return $results;
    }

    public function addFavourites($user_id,$note,$search_text,$search_results)
    {
        $f_id = $this->db->table('favourites')
            ->insertGetId(
                array(
                    'searchjson' => $search_text,
                    'notes' => $note ,
                    'created_by' => $user_id,
                    'created_at' => date("Y-m-d H:i:s")
                ));

        foreach($search_results as $search){

            $this->db->table('favourite_details')
                ->insert(
                    array(
                        'manufacturer' => $search['manufacturer'],
                        'f_id' => $f_id ,
                        'item_name' =>$search['product']['name'] ,
                        'item_gen' => $search['product']['genericMedicine'],
                        'item_form' => $search['product']['form'] ,
                        'created_by' => $user_id,
                        'created_at' => date("Y-m-d H:i:s")
                    ));

        }

        return true;
    }



    public function deleteFavourite($favourite_id)
    {

        $this->db->table('favourite_details')
            ->where('f_id', $favourite_id)
            ->delete();

        $this->db->table('favourites')
            ->where('id', $favourite_id)
            ->delete();

        return true;
    }

    public function deleteFavouriteItem($favourite_item_id)
    {
        $this->db->table('favourite_details')
            ->where('id', $favourite_item_id)
            ->delete();

        return true;
    }



}