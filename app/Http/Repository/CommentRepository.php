<?php

namespace App\Http\Repository;


class CommentRepository
{
    /** @var DB */
    protected $db;


    public function __construct()
    {
        $this->db = \DB::connection('mysql');
    }

    public function getComments($topticId)
    {

        $query = $this->db->table('replies')
            ->select(array(
                $this->db->raw('replies.id'),
                $this->db->raw('replies.created_by'),
                $this->db->raw('replies.commenttext'),
                $this->db->raw('users.fullname')
            ))
            ->join('users', 'replies.created_by', '=', 'users.id')
            ->where('replies.topicid', '=', $topticId);;

        $results = $query->get();
        return $results;
    }

    public function getComment($commentId)
    {
        $query = $this->db->table('replies')
            ->select(array(
                $this->db->raw('replies.id'),
                $this->db->raw('replies.created_by'),
                $this->db->raw('replies.commenttext'),
                $this->db->raw('users.fullname')
            ))
            ->join('users', 'replies.created_by', '=', 'users.id')
            ->where('replies.id', '=', $commentId);

        $results = $query->first();

        return $results;
    }

    public function saveComment($params)
    {
        if(isset($params["id"]) && $params["id"]>0){
            return $this->updateComment($params);
        }

        $id = $this->db->table('replies')
            ->insertGetId(
                array(
                    'commenttext' => $params['commenttext'],
                    'topicid' => $params['topicid'],
                    'created_by' => $params['created_by'],
                    'created_at' => date("Y-m-d H:i:s")
                ));


        return $this->getComment($id);
    }

    public function updateComment($params)
    {
        $this->db->table('replies')
            ->where('id', $params["id"])
            ->update(
                array(
                    'commenttext' => $params['commenttext']
                ));

        return $this->getComment($params["id"]);
    }

    public function deleteComment($commentId)
    {
        $this->db->table('replies')
            ->where('id', $commentId)
            ->delete();

        return true;
    }

}