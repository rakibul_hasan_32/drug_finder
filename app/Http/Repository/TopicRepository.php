<?php

namespace App\Http\Repository;


class TopicRepository
{
    /** @var DB */
    protected $db;


    public function __construct()
    {
        $this->db = \DB::connection('mysql');
    }

    public function getTopics()
    {

        $query = $this->db->table('topics')
            ->select(array(
                $this->db->raw('topics.id'),
                $this->db->raw('topics.created_by'),
                $this->db->raw('topics.topic'),
                $this->db->raw('topics.details')
            ))
            ->join('users', 'topics.created_by', '=', 'users.id');

        $results = $query->get();
        return $results;
    }

    public function getTopic($topicId)
    {
        $query = $this->db->table('topics')
            ->select(array(
                $this->db->raw('topics.id'),
                $this->db->raw('topics.created_by'),
                $this->db->raw('topics.topic'),
                $this->db->raw('topics.details')
            ))
            ->join('users', 'topics.created_by', '=', 'users.id')
            ->where('topics.id', '=', $topicId);

        $results = $query->first();

        return $results;
    }

    public function saveTopic($params)
    {
        if(isset($params["id"]) && $params["id"]>0){
            return $this->updateTopic($params);
        }

        $id = $this->db->table('topics')
            ->insertGetId(
                array(
                    'topic' => $params['topic'],
                    'details' => $params['details'],
                    'created_by' => $params['created_by'],
                    'created_at' => date("Y-m-d H:i:s")
                ));


        return $this->getTopic($id);
    }

    public function updateTopic($params)
    {
        $this->db->table('topics')
            ->where('id', $params["id"])
            ->update(
                array(
                    'topic' => $params['topic'],
                    'details' => $params['details']
                ));

        return $this->getComment($params["id"]);
    }

    public function deleteTopic($topicId)
    {
        $this->db->table('replies')
            ->where('topicid', $topicId)
            ->delete();


        $this->db->table('topics')
            ->where('id', $topicId)
            ->delete();

        return true;
    }

}