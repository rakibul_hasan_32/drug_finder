@extends('layouts.app')

@section('content')
    <h1>{{$header}}</h1>

    @include('layouts.partials.errors')

    <ul data-role="listview" data-split-icon="delete" data-split-theme="a" >
        @forelse($favouriteslist as $product)
            <li id="{{ $product->id  }}">
                <a href="#">
                    <h2>{{$product->item_name}}</h2>
                    <p>"{{$product->item_gen}}" form of {{$product->item_form}}</p>
                    <p>
                        <strong>{{$product->manufacturer}}</strong>
                    </p>
                </a>
                <a href="#favouritedetails" data-rel="popup" data-position-to="window" data-transition="pop">del</a>

            </li>
        @empty
            <p class="no_data">No Data Found</p>
        @endforelse
    </ul>

    <div data-role="popup" id="favouritedetails" data-theme="a" data-overlay-theme="b" class="ui-content" style="max-width:340px; padding-bottom:2em;">
        <h3>Delete Item?</h3>
        <p>If you delete the item then it will be gone for ever, so please be sure before deleting any record.</p>
        <a href="#delFavouriteItem" data-rel="back" class="ui-shadow ui-btn ui-corner-all ui-btn-b ui-icon-check ui-btn-icon-left ui-btn-inline ui-mini">Delete</a>
        <a href="#" data-rel="back" class="ui-shadow ui-btn ui-corner-all ui-btn-inline ui-mini">Cancel</a>
    </div>


@stop

