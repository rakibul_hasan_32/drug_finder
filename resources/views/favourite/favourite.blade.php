@extends('layouts.app')

@section('content')

    @include('layouts.partials.errors')

    <ul data-role="listview"  data-split-icon="delete" data-split-theme="a">
        @forelse($favouritelist as $favourite)
            <li id="{{ $favourite->id  }}" >
                <a href="/favoritedetails/{{ $favourite->id  }}">
                    <h2>{{$favourite->notes }}</h2>
                    <p>
                        <strong>Createed At : {{ $favourite->created_at }}</strong>
                    </p>
                </a>
                <a href="#favourite" data-rel="popup" data-position-to="window" data-transition="pop">del</a>
            </li>
        @empty
            <p class="no_data">No Data Found</p>
        @endforelse
    </ul>

    <div data-role="popup" id="favourite" data-theme="a" data-overlay-theme="b" class="ui-content" style="max-width:340px; padding-bottom:2em;">
        <h3>Delete Item?</h3>
        <p>If you delete the item then it will be gone for ever, so please be sure before deleting any record.</p>
        <a href="#delFavourite" data-rel="back" class="ui-shadow ui-btn ui-corner-all ui-btn-b ui-icon-check ui-btn-icon-left ui-btn-inline ui-mini">Delete</a>
        <a href="#" data-rel="back" class="ui-shadow ui-btn ui-corner-all ui-btn-inline ui-mini">Cancel</a>
    </div>


@stop