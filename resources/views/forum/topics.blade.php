@extends('layouts.app')

@section('content')


        @include('layouts.partials.errors')


    <ul class="topic_list" data-role="listview" data-split-icon="delete" >
        <li data-role="list-divider" role="heading">

            <h2 class="list-header">Topics </h2>
            <div   style="color:white;float:right;margin-top:-35px;margin-right: 10px;">
                <a href="#add_topic" data-rel="popup" data-position-to="window"
                   data-transition="pop"  class="ui-btn">
                    Add
                </a>
            </div>

        </li>
        @if($topicList!=null && sizeof($topicList)>0)
            @foreach($topicList as $topic)
                @if(Auth::user()->id == $topic->created_by)
                    <li id="topic_id_{{$topic->id}}"><a data-ajax='false' href="{!! url('comments/'.$topic->id) !!}"><h2>{{$topic->topic}} </h2><p><strong class="topic_text">{{$topic->details}}</strong></p></a><a onclick="javascript:deleteConfirm(this);" href="javascript:void(0);" id="{{$topic->id}}" >Delete topic</a></li>
                @else
                    <li id="topic_id_{{$topic->id}}"><a data-ajax='false' class="ui-state-disabled" href="{!! url('comments/'.$topic->id) !!}"><h2>{{$topic->topic}} </h2><p><strong class="topic_text">{{$topic->details}}</strong></p></a><a class="ui-state-disabled" href="javascript:void(0);" id="{{$topic->id}}" >Delete topic</a></li>
                @endif
            @endforeach
        @else
            <p id="no_data">No Data Found</p>
        @endif
    </ul>

    <div data-role="popup" id="delete_topic" class="ui-content"
         style="max-width:340px; padding-bottom:2em;">
            <h1>Delete topic?</h1>
        <h3 class="ui-title">Are you sure you want to delete this topic?</h3>            
            <a onclick="javascript:deleteTopic(this);" href="javascript:void(0);" data-rel="back"
               class="delete_topic ui-shadow ui-btn ui-corner-all ui-btn-b ui-icon-check ui-btn-icon-left ui-btn-inline ui-mini">Delete</a>
            <a href="javascript:void(0);" data-rel="back"
               class="ui-shadow ui-btn ui-corner-all ui-btn-inline ui-mini">Cancel</a>
    </div>
    <div data-role="popup" id="add_topic" class="ui-content"
         style="max-width:340px; padding-bottom:2em;">
            <h1>Add Topic</h1>    
            <label for="topictext">Topic:</label>
        <textarea cols="40" rows="8" name="topictext" id="topictext"></textarea>
        <p class="topic_text_error hidden red_color">Topic text cannot be empty</p>
        <label for="topicdetails">Topic details:</label>
        <textarea cols="40" rows="8" name="topicdetails" id="topicdetails"></textarea>
        <p class="topic_details_error hidden red_color">Topic details cannot be empty</p>        
            <a href="javascript:void(0);" onclick="return saveTopic(this);"
               class="save_topic ui-shadow ui-btn ui-corner-all ui-btn-b ui-icon-check ui-btn-icon-left ui-btn-inline ui-mini">Save</a>
            <a id="close_save_popup" href="javascript:void(0);" data-rel="back"
               class="ui-shadow ui-btn ui-corner-all ui-btn-inline ui-mini">Cancel</a>

    </div>
@stop
<script src="/js/topics.js" type="text/javascript"></script>