@extends('layouts.app')

@section('content')

    @include('layouts.partials.errors')

    <input id="topicId" type="hidden" value="{{$topic->id}}"/>
    <ul class="comment_list" data-role="listview" data-split-icon="delete" >

        <li data-role="list-divider" role="heading">

            <h1>{{$topic->topic}}</h1>
            <div   style="color:white;float:right;margin-top:-35px;margin-right: 10px;">
                <a href="#add_comment" data-rel="popup" data-position-to="window"
                   data-transition="pop"  class="ui-btn">
                    Add
                </a>
            </div>
        </li>

    @if($commentList!=null && sizeof($commentList)>0)
            @foreach($commentList as $comment)
                @if(Auth::user()->id == $comment->created_by)
                    <li id="comment_id_{{$comment->id}}"><a onclick="editComment(this,'{{$comment->id}}')" href="javascript:void(0);"><h2>{{$comment->fullname}} says: </h2><p><strong class="comment_text">{{$comment->commenttext}}</strong></p></a><a onclick="javascript:deleteConfirm(this);" href="javascript:void(0);" id="{{$comment->id}}" >Delete Comment</a></li>
                @else
                    <li id="comment_id_{{$comment->id}}"><a class="ui-state-disabled" href="javascript:void(0);"><h2>{{$comment->fullname}} says: </h2><p><strong class="comment_text">{{$comment->commenttext}}</strong></p></a><a class="ui-state-disabled" href="javascript:void(0);" id="{{$comment->id}}" >Delete Comment</a></li>
                @endif
            @endforeach
        @else
            <p id="no_data">No Data Found</p>
        @endif
    </ul>

    <div data-role="popup" id="delete_comment" class="ui-content"
         style="max-width:340px; padding-bottom:2em;">
            <h1>Delete Comment?</h1>
            <h3 class="ui-title">Are you sure you want to delete this comment?</h3>            
            <a onclick="javascript:deleteComment(this);" href="javascript:void(0);" data-rel="back"
               class="delete_comment ui-shadow ui-btn ui-corner-all ui-btn-b ui-icon-check ui-btn-icon-left ui-btn-inline ui-mini">Delete</a>
            <a href="javascript:void(0);" data-rel="back"
               class="ui-shadow ui-btn ui-corner-all ui-btn-inline ui-mini">Cancel</a>
    </div>
    <div data-role="popup" id="add_comment" class="ui-content"
         style="max-width:340px; padding-bottom:2em;">
            <h1>Add Comment</h1>    
            <label for="commenttext">Comment text:</label>
            <textarea cols="40" rows="8" name="commenttext" id="commenttext"></textarea>
            <p class="comment_text_error hidden red_color">Comment text cannot be empty</p>    
            <a href="javascript:void(0);" onclick="return saveComment(this);"
               class="save_comment ui-shadow ui-btn ui-corner-all ui-btn-b ui-icon-check ui-btn-icon-left ui-btn-inline ui-mini">Save</a>
            <a id="close_save_popup" href="javascript:void(0);" data-rel="back"
               class="ui-shadow ui-btn ui-corner-all ui-btn-inline ui-mini">Cancel</a>

    </div>
@stop
<script src="/js/comments.js" type="text/javascript"></script>