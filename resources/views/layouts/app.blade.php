<!DOCTYPE html>
<html lang="en">
<head>

    <title>Drug Finder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="/js/jq/jquery.mobile-1.4.5.min.css"/>
    <link rel="stylesheet" href="/js/jq/jquery.mobile.theme-1.4.5.min.css"/>
    <link rel="Stylesheet" href="/css/style.css"/>
    <link rel="Stylesheet" href="/css/all.css"/>
    <script type="text/javascript" src="/js/jq/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/js/jq/jquery.mobile-1.4.5.min.js"></script>
    <script src="/js/mobile-init.js" type="text/javascript"></script>
</head>
<body>

<div data-role="page">
    @include("layouts.partials.pageheader")

    <div role="main" class="ui-content">
        @yield('content')
    </div>

    <footer data-role="footer" class="footer">
        <div class="container text-center">
            <p class="pull-left"> © 2015 Company, Inc. All Rights Reserved</p>
        </div>
    </footer>
</div>

<script src="/js/favourites.js" type="text/javascript"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>
</body>
</html>