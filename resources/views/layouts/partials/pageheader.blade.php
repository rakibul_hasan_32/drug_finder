<div data-role="header" data-position="fixed" class="ui-header ui-bar-a" role="banner">
    <div class="inner">
        <a href="/"><h1 class="logo navbar-brand">Drug finder</h1></a>
        <form id="frmsearch" data-ajax="false"  action="/search" method="post">
            {!! csrf_field() !!}
            <label for="searchbar" class="ui-hidden-accessible">Search:</label>
            <input type="search" name="term" id="term" value="" data-theme="d" data-mini="true" />
            <div id="icon_holder" class="">
                @if (Auth::guest())
                <a data-ajax="false" href="{{ route('auth.login') }}"  class="ui-btn ui-shadow ui-corner-all ui-icon-user ui-btn-icon-notext">user</a>
                <a data-ajax="false" href="{{ route('auth.register') }}" class="ui-btn ui-shadow ui-corner-all ui-icon-plus ui-btn-icon-notext">addition</a>
                @else
                    <a data-ajax="false" href="{{ route('logout') }}" class="ui-btn ui-shadow ui-corner-all ui-icon-logout ui-btn-icon-notext">minus</a>
                    <a data-ajax="false" href="{{ route('account.dashboard') }}"  class="ui-btn ui-shadow ui-corner-all ui-icon-bullets ui-btn-icon-notext">user</a>
                @endif
            </div>
        </form>

    </div>

    <div data-role="navbar" class="ui-navbar ui-mini" role="navigation">
        <ul>
            <li>
                <a data-ajax="false" href="{!! url('home') !!}" data-corners="false" data-shadow="false" data-iconshadow="true"
                   data-wrapperels="span" data-theme="a" data-inline="true"
                   class="ui-btn ui-btn-inline ui-btn-up-a {{ Request::is('/') || Request::is('home') || Request::is('search')  ? 'ui-btn-active ui-state-persist' : '' }}">Drugs
                </a>
            </li>
            <li>
                <a data-ajax="false" href="{!! url('favorites') !!}" data-corners="false" data-shadow="false" data-iconshadow="true"
                   data-wrapperels="span" data-theme="a" data-inline="true" class="ui-btn ui-btn-inline ui-btn-up-a {{ Request::is('favorites') || Request::is('favoritedetails/*')  ? 'ui-btn-active ui-state-persist' : '' }}">
                    Favourites
                </a>
            </li>
            <li>
                <a data-ajax="false" href="{!! url('topics') !!}" data-corners="false" data-shadow="false" data-iconshadow="true"
                   data-wrapperels="span" data-theme="a" data-inline="true" class="ui-btn ui-btn-inline ui-btn-up-a {{ Request::is('topics') || Request::is('comments/*') ? 'ui-btn-active ui-state-persist' : '' }}">
                    Forums
                </a>
            </li>
        </ul>
    </div><!-- /navbar -->
</div>