@extends('layouts.app')

@section('content')
    <div data-role="content">
        @if(auth()->guest())
        Data will be come here
        @else
            <h1>Hello {{ Auth::user()->fullname }}</h1>
        @endif
    </div>
@stop
