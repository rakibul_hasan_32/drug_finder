@extends('layouts.app')

@section('content')
    <h1>{{$header}}</h1>

    @include('layouts.partials.errors')
    @if(Auth::user() != null)
        <a  data-rel="popup" data-position-to="window" data-transition="pop"  class="ui-btn" href="#add_to_favourite">Add To Favourite</a>
    @endif
        <ul data-role="listview">
            @forelse($productList as $product)
                <li><a href="#">
                    <h2>{{$product['item']}}</h2>
                    <p>"{{$product['genericMedicine']}}" form of {{$product['form']}}</p>
                    <p><strong>{{$product['manufacturer']}}</strong></p>
                    </a>
                </li>
            @empty
                <p class="no_data">No Data Found</p>
            @endforelse
        </ul>
        <div data-role="popup" id="add_to_favourite" class="ui-content"
             style="max-width:340px; padding-bottom:2em;">
            <form data-ajax="false" method="POST" action="{{ url('favorites') }}" onsubmit="return validateFavourite();">
                <h1>Add To Favourite</h1>    
                <input type="hidden" name="term" value="{{$search_term}}"/>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <label for="note">Note:</label>
            <textarea cols="40" rows="8" name="note" id="note"></textarea>
            <p class="note_text_error hidden red_color">Note cannot be empty</p>    
                 <button type="submit" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-btn-icon-left ui-icon-check">Save</button>            
            </form>
        </div>
    @stop